pelican-uberspot is a [pelican](https://blog.getpelican.com) theme inspired by <http://uberspot.github.io/>.
Many stylistic changes are taken from <http://vincent.bernat.im/>.

## Variables

## Plugins

## License

Everything is licensed under MIT, except otherwise stated.

## Screenshots

**Index page**

![Index page](uberspot_index.png)

**Single article**

![Single article](uberspot_article.png)

**Archives**

![Archives](uberspot_archives.png)

**Search**

![Archives](uberspot_search.png)
